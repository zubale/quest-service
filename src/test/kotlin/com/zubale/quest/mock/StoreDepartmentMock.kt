package com.zubale.quest.mock

import com.zubale.quest.document.StoreDepartment
import org.bson.types.ObjectId
import java.util.Locale

object StoreDepartmentMock {

    fun demoStoreDepartment(): StoreDepartment {
        return StoreDepartment(
                id = ObjectId(),
                name = "Helados",
                locale = Locale("es", "MX")
        )
    }

}
