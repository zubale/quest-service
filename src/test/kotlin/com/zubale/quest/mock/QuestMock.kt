package com.zubale.quest.mock

import com.zubale.quest.document.Quest
import com.zubale.quest.domain.zombie.Company
import com.zubale.quest.domain.zombie.Retailer
import com.zubale.quest.representation.BrandRepresentation
import com.zubale.quest.representation.CompletionRepresentation
import com.zubale.quest.representation.CycleRepresentation
import com.zubale.quest.representation.enumerator.QuestType
import com.zubale.quest.representation.GoogleFormRepresentation
import com.zubale.quest.representation.StoreDepartmentRepresentation
import com.zubale.quest.representation.StoreRepresentation
import com.zubale.quest.representation.enumerator.PromoterType
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.UUID

object QuestMock {

    fun demo(): Quest {
        return Quest(
                id = ObjectId(),
                name = "Quest test",
                description = "Quest description",
                type = QuestType.INVENTORY,
                brand = BrandRepresentation(
                        id = 1,
                        zubaleId = 1,
                        name = "Brand",
                        logoUrl = "some logo"
                ),
                duration = "1 min",
                country = "MX",
                cycle = CycleRepresentation(
                        startDate = LocalDateTime.now(),
                        endDate = LocalDateTime.now()
                ),
                department = StoreDepartmentRepresentation(
                        id = ObjectId(),
                        name = "Frozen"
                ),
                location = GeoJsonPoint(2.0, 1.0),
                rewardAmount = BigDecimal.TEN,
                googleForm = GoogleFormRepresentation(
                        url = "URL",
                        phoneNumberFieldKey = "key",
                        questFieldKey = "key",
                        storeFieldKey = "key"
                ),
                store = StoreRepresentation(
                        id = 1,
                        zubaleId = 10,
                        name = "Some store",
                        zip = "11529",
                        storeNumber = "123",
                        state = "Mex",
                        neighborhood = "Polanco",
                        city = "Mex",
                        address = "Eugenio Sue",
                        country = "MX",
                        mapUrl = "map",
                        retailer = Retailer(
                                id = 1,
                                name = "Some retailer",
                                zubaleId = 2,
                                company = Company(
                                        id = 1,
                                        name = "Zubale",
                                        zubaleId = 2
                                ),
                                logoUrl = "some logo"
                        )
                )
        )
    }

    fun demoClosed(): Quest {
        return demo().copy(closed = true)
    }

    fun demoOpen(): Quest {
        return demo().copy(closed = false)
    }

    fun demoCompletionRequest(): CompletionRepresentation {
        return CompletionRepresentation(
                userId = UUID.randomUUID(),
                bonusAmount = BigDecimal.ONE,
                promoterType = PromoterType.PROMOTER
        )
    }

}
