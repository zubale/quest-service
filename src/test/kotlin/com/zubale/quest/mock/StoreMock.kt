package com.zubale.quest.mock

import com.zubale.quest.domain.zombie.Company
import com.zubale.quest.domain.zombie.Retailer
import com.zubale.quest.domain.zombie.Store
import com.zubale.quest.domain.zombie.StoreArea
import com.zubale.quest.domain.zombie.StorePoint

object StoreMock {

    fun demo(): Store {
        return Store(
                id = 1,
                name = "Some store",
                zubaleId = 1,
                country = "MX",
                mapUrl = "url",
                address = "Some address",
                city = "CDMX",
                neighborhood = "Polanco",
                zip = "11529",
                storeNumber = "123",
                state = "CDMX",
                point = StorePoint(
                        latitude = 1.0,
                        longitude = 1.0
                ),
                retailer = Retailer(
                        id = 1,
                        name = "Some retailer",
                        zubaleId = 2,
                        company = Company(
                                id = 1,
                                name = "Zubale",
                                zubaleId = 2
                        ),
                        logoUrl = "some logo"
                ),
                zubaleArea = StoreArea(
                        id = 1,
                        name = "Area"
                )
        )
    }

}
