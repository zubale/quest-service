package com.zubale.quest.controller

import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc

@RunWith(SpringRunner::class)
abstract class BaseControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

}
