package com.zubale.quest.controller.private

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.zubale.quest.configuration.JacksonConfiguration
import com.zubale.quest.configuration.SecurityConfiguration
import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.controller.BaseControllerTest
import com.zubale.quest.mock.QuestMock
import com.zubale.quest.service.QuestService
import org.bson.types.ObjectId
import org.junit.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.UUID
import java.util.Optional.of

@Import(value = [JacksonConfiguration::class, SecurityConfiguration::class, SecurityProperties::class])
@ComponentScan(basePackages = ["com.zubale.quest.security"])
@WebMvcTest(PrivateQuestController::class)
class PrivateQuestControllerTest : BaseControllerTest() {

    @MockBean
    private lateinit var questService: QuestService

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `should close quest`() {
        val quest = QuestMock.demoOpen()

        given(questService.findBy(any<ObjectId>())).willReturn(of(quest))

        mvc.perform(patch("/private/v1/quests/${quest.id}/close")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent)
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `should open quest`() {
        val quest = QuestMock.demoClosed()

        given(questService.findBy(any<ObjectId>())).willReturn(of(quest))

        mvc.perform(patch("/private/v1/quests/${quest.id}/open")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNoContent)
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `should complete a quest throws minimum bonus amount exceeded exception`() {
        val quest = QuestMock.demo()

        given(questService.findBy(any<ObjectId>())).willReturn(of(quest))

        mvc.perform(post("/private/v1/quests/${quest.id}/complete")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""
                    {
                      "userId": "${UUID.randomUUID().toString()}",
                      "bonusAmount": -1,
                      "promoterType": "PROMOTER"
                    }
                """.trimIndent()))
                .andExpect(status().isBadRequest)
                .andExpect(jsonPath("$.messages").value("bonusAmount, must be greater than or equal to 0"))
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `should complete a quest throws maximum bonus amount exceeded exception`() {
        val quest = QuestMock.demo()

        given(questService.findBy(any<ObjectId>())).willReturn(of(quest))

        mvc.perform(post("/private/v1/quests/${quest.id}/complete")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""
                    {
                      "userId": "${UUID.randomUUID().toString()}",
                      "bonusAmount": 5001,
                      "promoterType": "PROMOTER"
                    }
                """.trimIndent()))
                .andExpect(status().isBadRequest)
                .andExpect(jsonPath("$.messages").value("bonusAmount, must be less than or equal to 5000"))
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `should complete a quest`() {
        val quest = QuestMock.demo()

        given(questService.findBy(any<ObjectId>())).willReturn(of(quest))

        mvc.perform(post("/private/v1/quests/${quest.id}/complete")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("""
                    {
                      "userId": "${UUID.randomUUID().toString()}",
                      "bonusAmount": 600,
                      "promoterType": "PROMOTER"
                    }
                """.trimIndent()))
                .andExpect(status().isAccepted)
    }

}
