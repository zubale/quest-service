package com.zubale.quest.controller.private

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.zubale.quest.configuration.JacksonConfiguration
import com.zubale.quest.configuration.SecurityConfiguration
import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.controller.BaseControllerTest
import com.zubale.quest.mock.StoreMock
import com.zubale.quest.service.ZombieService
import org.junit.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@Import(value = [JacksonConfiguration::class, SecurityConfiguration::class, SecurityProperties::class])
@ComponentScan(basePackages = ["com.zubale.quest.security"])
@WebMvcTest(PrivateStoreController::class)
class PrivateStoreControllerTest : BaseControllerTest() {

    @MockBean
    private lateinit var zombieService: ZombieService

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `should return all departments`() {
        val stores = mutableListOf(StoreMock.demo())
        given(zombieService.getStoresByZubaleIds(any())).willReturn(stores)

        mvc.perform(post("/private/v1/stores/bulk/zubale-identifier")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("[1,2,3]"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$.length()").value(stores.size))
    }

}
