package com.zubale.quest.controller

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.zubale.quest.configuration.JacksonConfiguration
import com.zubale.quest.configuration.SecurityConfiguration
import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.mock.StoreDepartmentMock
import com.zubale.quest.service.StoreService
import org.hamcrest.Matchers.hasSize
import org.junit.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@Import(value = [JacksonConfiguration::class, SecurityConfiguration::class, SecurityProperties::class])
@ComponentScan(basePackages = ["com.zubale.quest.security"])
@WebMvcTest(StoreController::class)
class StoreControllerTest : BaseControllerTest() {

    @MockBean
    private lateinit var storeService: StoreService

    @Test
    @WithMockUser(roles = ["USER"])
    fun `should return all departments`() {
        val departments = mutableListOf(StoreDepartmentMock.demoStoreDepartment())
        given(storeService.getDepartments(any())).willReturn(departments)

        mvc.perform(MockMvcRequestBuilders.get("/public/v1/stores/departments")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$.length()").value(departments.size))
    }

}
