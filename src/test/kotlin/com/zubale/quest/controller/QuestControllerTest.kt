package com.zubale.quest.controller

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.zubale.quest.configuration.JacksonConfiguration
import com.zubale.quest.configuration.SecurityConfiguration
import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.mock.QuestMock
import com.zubale.quest.representation.filter.PageFilter
import com.zubale.quest.representation.filter.toPageRequest
import com.zubale.quest.representation.request.BrandRequest
import com.zubale.quest.representation.request.LocationRequest
import com.zubale.quest.service.QuestService
import org.bson.types.ObjectId
import org.junit.Test
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.data.domain.PageImpl
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.Optional.of
import org.hamcrest.Matchers.`is` as Is

@Import(value = [JacksonConfiguration::class, SecurityConfiguration::class, SecurityProperties::class])
@ComponentScan(basePackages = ["com.zubale.quest.security"])
@WebMvcTest(QuestController::class)
class QuestControllerTest : BaseControllerTest() {

    @MockBean
    private lateinit var questService: QuestService

    @Test
    @WithMockUser(roles = ["USER"])
    fun `should return a quest`() {
        val quest = QuestMock.demo()

        given(questService.findBy(any<ObjectId>())).willReturn(of(quest))

        mvc.perform(get("/public/v1/quests/${quest.id}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id", Is(quest.id.toHexString())))
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `should return a quest by location`() {
        val quests = mutableListOf(QuestMock.demo())
        val pageable = PageFilter().toPageRequest()
        val page = PageImpl(quests, pageable, quests.size.toLong())

        val locationRequest = LocationRequest(
                latitude = 12.12545,
                longitude = -124.4546,
                distance = 1.0
        )

        given(questService.findBy(any(), any())).willReturn(page)

        mvc.perform(get("/public/v1/quests/location?latitude=${locationRequest.latitude}&longitude=${locationRequest.longitude}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.content").isArray)
                .andExpect(jsonPath("$.totalSize").value(quests.size))
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `should return all quests`() {
        val quests = mutableListOf(QuestMock.demo())
        val pageable = PageFilter().toPageRequest()
        val page = PageImpl(quests, pageable, quests.size.toLong())

        given(questService.findBy(any<PageFilter>())).willReturn(page)

        mvc.perform(get("/public/v1/quests")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.content").isArray)
                .andExpect(jsonPath("$.totalSize").value(quests.size))
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `should return a quest by brand filtered by a range of cycle start date `() {
        val quests = mutableListOf(QuestMock.demo())

        val brandRequest = BrandRequest(
                zubaleId = 1000,
                fromDate = "2019-10-01T00:00:00.00",
                toDate = "2019-10-31T00:00:00.00"
        )

        given(questService.findByBrandFilter(any())).willReturn(quests)

        mvc.perform(get("/public/v1/quests/brands-filter?zubaleId=${brandRequest.zubaleId}&fromDate=${brandRequest.fromDate}&toDate=${brandRequest.toDate}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0]").isNotEmpty)
                .andExpect(jsonPath("$[0]").value(10))
    }

}
