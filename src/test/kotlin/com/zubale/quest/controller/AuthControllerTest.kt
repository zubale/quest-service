package com.zubale.quest.controller

import com.nhaarman.mockitokotlin2.given
import com.zubale.quest.configuration.SecurityConfiguration
import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.representation.TokenRepresentation
import com.zubale.quest.representation.enumerator.TokenType.BEARER
import com.zubale.quest.service.AuthService
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.anyString
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.hamcrest.Matchers.`is` as Is

@Import(value = [SecurityConfiguration::class, SecurityProperties::class])
@ComponentScan(basePackages = ["com.zubale.quest.security"])
@WebMvcTest(AuthController::class)
class AuthControllerTest : BaseControllerTest() {

    @MockBean
    private lateinit var authService: AuthService

    @Test
    fun `should return a token`() {
        val token = TokenRepresentation(BEARER, "token")
        given(authService.login(anyString())).willReturn(token)

        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header(HttpHeaders.AUTHORIZATION, "auth"))
                .andExpect(status().isAccepted)
                .andExpect(jsonPath("$.token", Is(token.token)))
    }

    @Test
    fun `should return a forbidden access`() {
        mvc.perform(get("/any/path")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden)
    }

}
