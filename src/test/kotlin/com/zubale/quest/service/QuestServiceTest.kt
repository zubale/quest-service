package com.zubale.quest.service

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.zubale.quest.configuration.properties.QuestProperties
import com.zubale.quest.document.Quest
import com.zubale.quest.exception.QuestCompletedException
import com.zubale.quest.exception.ResourceNotFoundException
import com.zubale.quest.mock.QuestMock
import com.zubale.quest.repository.QuestRepository
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.springframework.test.context.junit4.SpringRunner
import java.lang.Boolean.TRUE
import java.util.Optional
import java.util.Optional.of

@RunWith(SpringRunner::class)
class QuestServiceTest {

    @Mock
    private lateinit var questRepository: QuestRepository

    @Mock
    private lateinit var questProperties: QuestProperties

    @Mock
    private lateinit var zombieService: ZombieService

    @Mock
    private lateinit var storeService: StoreService

    @Mock
    private lateinit var redemptionService: RedemptionService

    private lateinit var questService: QuestService

    @Before
    fun setup() {
        questService = QuestService(questProperties, questRepository, zombieService, storeService, redemptionService)
    }

    @Test
    fun `should close quest`() {
        val quest = QuestMock.demoOpen()
        given(questRepository.findById(quest.id)).willReturn(of(quest))

        Mockito.`when`(questRepository.save(any<Quest>())).thenAnswer {
            val savedQuest: Quest = it.getArgument(0)
            Assert.assertTrue(savedQuest.closed)
            savedQuest
        }

        questService.closeOrOpenQuest(quest.id, true)
    }

    @Test
    fun `should open quest`() {
        val quest = QuestMock.demoClosed()
        given(questRepository.findById(quest.id)).willReturn(of(quest))

        Mockito.`when`(questRepository.save(any<Quest>())).thenAnswer {
            val savedQuest: Quest = it.getArgument(0)
            Assert.assertFalse(savedQuest.closed)
            savedQuest
        }

        questService.closeOrOpenQuest(quest.id, false)
    }

    @Test(expected = ResourceNotFoundException::class)
    fun `should fail to find quest`() {
        val quest = QuestMock.demoClosed()
        given(questRepository.findById(any())).willReturn(Optional.empty())
        verify(questRepository, times(0)).save(any<Quest>())

        questService.closeOrOpenQuest(quest.id, false)
    }

    @Test(expected = ResourceNotFoundException::class)
    fun `should complete a quest throws no quest found exception`() {
        val quest = QuestMock.demo()
        val completion = QuestMock.demoCompletionRequest()

        given(questRepository.findById(any())).willReturn(Optional.empty())
        verify(questRepository, times(0)).save(any<Quest>())

        questService.completeQuest(quest.id, completion)
    }

    @Test
    fun `should complete a quest`() {
        val completion = QuestMock.demoCompletionRequest()
        val quest = QuestMock.demoClosed().copy(completion = completion)

        given(questRepository.findById(quest.id)).willReturn(of(quest))
        given(redemptionService.rewardQuest(any(), any())).willReturn(of(TRUE))

        Mockito.`when`(questRepository.save(any<Quest>())).thenAnswer {
            val savedQuest: Quest = it.getArgument(0)
            Assert.assertTrue(savedQuest.closed)
            Assert.assertEquals(savedQuest.completion, completion)
            savedQuest
        }

        questService.completeQuest(quest.id, completion)
    }

    @Test(expected = QuestCompletedException::class)
    fun `should throw an error on opened quest when complete a quest`() {
        val completion = QuestMock.demoCompletionRequest()
        val quest = QuestMock.demoOpen().copy(completion = completion)

        given(questRepository.findById(quest.id)).willReturn(of(quest))

        questService.completeQuest(quest.id, completion)
    }
}
