package com.zubale.quest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class QuestServiceApplication

fun main(args: Array<String>) {
    runApplication<QuestServiceApplication>(*args)
}
