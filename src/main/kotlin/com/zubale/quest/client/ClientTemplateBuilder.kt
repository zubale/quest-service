package com.zubale.quest.client

import com.zubale.quest.representation.TokenRepresentation
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.web.client.RestTemplate
import java.time.Duration
import java.time.temporal.ChronoUnit

const val DEFAULT_TIMEOUT = 15L

data class ClientTemplateBuilder(
        private val tokenRepresentation: TokenRepresentation,
        private val baseURL: String,
        private val timeout: Duration?,
        private val interceptors: List<ClientHttpRequestInterceptor>?
) {

    class Builder {
        private var timeout: Duration? = Duration.of(DEFAULT_TIMEOUT, ChronoUnit.SECONDS)
        private lateinit var tokenRepresentation: TokenRepresentation
        private lateinit var url: String
        private var interceptors: List<ClientHttpRequestInterceptor>? = null

        fun token(tokenRepresentation: TokenRepresentation) = apply { this.tokenRepresentation = tokenRepresentation }
        fun timeout(timeout: Duration) = apply { this.timeout = timeout }
        fun baseURL(url: String) = apply { this.url = url }
        fun interceptors(interceptors: List<ClientHttpRequestInterceptor>) = apply { this.interceptors = interceptors }

        fun build(): RestTemplate {
            return ClientTemplateBuilder(tokenRepresentation, url, timeout, interceptors).build()
        }
    }

    private fun interceptors(): List<ClientHttpRequestInterceptor> {
        val bearerInterceptor = ClientTokenInterceptor(tokenRepresentation)
        return listOf(bearerInterceptor)
    }

    private fun build(): RestTemplate {
        return RestTemplateBuilder()
                .rootUri(baseURL)
                .setConnectTimeout(timeout)
                .setReadTimeout(timeout)
                .interceptors(interceptors ?: interceptors())
                .errorHandler(ClientErrorHandler())
                .build()
    }

}
