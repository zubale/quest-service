package com.zubale.quest.client

import com.zubale.quest.representation.TokenRepresentation
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse

class ClientTokenInterceptor(private val tokenRepresentation: TokenRepresentation) : ClientHttpRequestInterceptor {

    override fun intercept(request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution): ClientHttpResponse {
        val token = StringBuilder(tokenRepresentation.type.code)
                .append(" ")
                .append(tokenRepresentation.token)
                .toString()

        val headers = request.headers
        headers.set(HttpHeaders.AUTHORIZATION, token)

        return execution.execute(request, body)
    }

}
