package com.zubale.quest.client.redemption

import com.zubale.quest.client.ClientException
import com.zubale.quest.domain.redemption.RewardRequest
import mu.KotlinLogging
import org.springframework.web.client.RestTemplate
import java.lang.Boolean.TRUE
import java.util.Optional
import java.util.Optional.empty
import java.util.Optional.of
import java.util.UUID

class RedemptionClientImpl(private val adminTemplate: RestTemplate) : RedemptionClient {

    private val log = KotlinLogging.logger {}

    override fun rewardQuest(userId: UUID, rewardRequest: RewardRequest): Optional<Boolean> {
        val url = "/admin/v2/users/$userId/reward"

        var isOk = of(TRUE)

        try {
            adminTemplate.postForObject(url, rewardRequest, Void::class.java)
        } catch (e: ClientException) {
            log.error("Error creating a reward [${rewardRequest.questId}]. Details {}", e.message)
            isOk = empty()
        }

        return isOk
    }

}
