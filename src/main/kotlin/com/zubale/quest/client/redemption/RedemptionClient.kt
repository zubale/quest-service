package com.zubale.quest.client.redemption

import com.zubale.quest.domain.redemption.RewardRequest
import java.util.Optional
import java.util.UUID

interface RedemptionClient {

    fun rewardQuest(userId: UUID, rewardRequest: RewardRequest): Optional<Boolean>

}
