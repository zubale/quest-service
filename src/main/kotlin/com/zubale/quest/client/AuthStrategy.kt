package com.zubale.quest.client

import com.zubale.quest.client.zombie.auth.ZombieAuthClient
import com.zubale.quest.domain.CacheNames
import com.zubale.quest.domain.zombie.AuthToken
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Component

@Component
class AuthStrategy(private val zombieAuthClient: ZombieAuthClient) {

    @Cacheable(cacheNames = [CacheNames.ZOMBIE_AUTH_TOKEN])
    fun getZombieToken(username: String, password: String): AuthToken {
        return zombieAuthClient.retrieveToken(username, password).orElseThrow { ClientAuthException("It's not possible login to zombie service") }
    }

}
