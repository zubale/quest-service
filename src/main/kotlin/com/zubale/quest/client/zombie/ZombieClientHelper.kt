package com.zubale.quest.client.zombie

import com.zubale.quest.client.AuthStrategy
import com.zubale.quest.client.ClientTemplateBuilder
import com.zubale.quest.client.zombie.base.ZombieClient
import com.zubale.quest.client.zombie.base.ZombieClientImpl
import com.zubale.quest.configuration.properties.ZombieClientProperties
import com.zubale.quest.representation.TokenRepresentation
import com.zubale.quest.representation.enumerator.TokenType
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component

@Component
class ZombieClientHelper(
        private val authStrategy: AuthStrategy,
        private val zombieClientProperties: ZombieClientProperties
) {

    @Scope(BeanDefinition.SCOPE_SINGLETON)
    fun instance(): ZombieClient {
        val authToken = authStrategy.getZombieToken(zombieClientProperties.username, zombieClientProperties.password)
        val token = TokenRepresentation(TokenType.JWT, authToken.token)
        val template = ClientTemplateBuilder.Builder()
                .baseURL(zombieClientProperties.baseURL)
                .token(token)
                .build()

        return ZombieClientImpl(template)
    }

}
