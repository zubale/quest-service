package com.zubale.quest.client.zombie.base

import com.zubale.quest.domain.zombie.Brand
import com.zubale.quest.domain.zombie.Store
import java.util.Optional

interface ZombieClient {

    fun getStores(): List<Store>
    fun getStoresByZubaleIds(zubaleIds: List<Long>): List<Store>
    fun getStore(id: Long): Optional<Store>
    fun getBrand(id: Long): Optional<Brand>

}
