package com.zubale.quest.client.zombie.auth

import com.zubale.quest.client.ClientException
import com.zubale.quest.domain.zombie.AuthToken
import com.zubale.quest.util.JsonUtil.toJson
import mu.KotlinLogging
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.client.RestTemplate
import java.util.Optional
import java.util.Optional.empty

class ZombieAuthClientImpl(private val authTemplate: RestTemplate) : ZombieAuthClient {

    private val log = KotlinLogging.logger {}

    override fun retrieveToken(username: String, password: String): Optional<AuthToken> {
        val url = "/rest/v1/token/"

        var token = empty<AuthToken>()

        try {
            val headers = HttpHeaders()
            headers["Accept"] = MediaType.APPLICATION_JSON_UTF8_VALUE
            headers["Content-Type"] = MediaType.APPLICATION_JSON_UTF8_VALUE

            val map = HashMap<String, String>()
            map["username"] = username
            map["password"] = password

            val entity = HttpEntity<Any>(toJson(map), headers)

            token = Optional.ofNullable(authTemplate.postForObject(url, entity, AuthToken::class.java))
        } catch (e: ClientException) {
            log.warn("Error to connect with zombie service. Details {}", e.message)
        }

        return token
    }

}
