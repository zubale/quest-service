package com.zubale.quest.client.zombie.base

import com.zubale.quest.client.ClientException
import com.zubale.quest.domain.ZombieMapper
import com.zubale.quest.domain.zombie.Brand
import com.zubale.quest.domain.zombie.Store
import com.zubale.quest.exception.ResourceNotFoundException
import com.zubale.quest.util.JsonUtil.toJson
import mu.KotlinLogging
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.GET
import org.springframework.http.HttpMethod.POST
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.client.RestTemplate
import java.util.Collections
import java.util.Optional
import java.util.Optional.empty

class ZombieClientImpl(private val restTemplate: RestTemplate) : ZombieClient {

    private val log = KotlinLogging.logger {}

    override fun getStore(id: Long): Optional<Store> {
        val endpoint = "/rest/v1/zombie/store/$id/"

        var store = empty<Store>()

        try {
            val response = restTemplate.getForObject(endpoint, Store::class.java)
            store = Optional.ofNullable(response)
        } catch (e: ClientException) {
            log.error("Error getting a store [$id]. Details {}", e.message)
        }

        return store
    }

    override fun getStores(): List<Store> {
        val endpoint = "/rest/v1/zombie/store/"

        try {
            val response = restTemplate.exchange(endpoint, GET, null, object : ParameterizedTypeReference<List<Store>>() {})
            return response.body ?: emptyList<Store>()
        } catch (e: ClientException) {
            log.error("Error getting stores. Details {}", e.message)
        }

        return emptyList<Store>()
    }

    override fun getStoresByZubaleIds(zubaleIds: List<Long>): List<Store> {
        val endpoint = "/rest/v1/zombie/store/by-zids"

        try {
            val request = Collections.singletonMap("zubaleIds", zubaleIds)
            val json = toJson(request)
            val headers = HttpHeaders()
            headers.contentType = MediaType.APPLICATION_JSON

            val entity = HttpEntity<Any>(json, headers)
            val response = restTemplate.exchange(endpoint, POST, entity, object : ParameterizedTypeReference<List<Store>>() {})

            return response.body ?: emptyList<Store>()
        } catch (e: ClientException) {
            log.error("Error getting stores. Details {}", e.message)

            if (e.statusCode == HttpStatus.NOT_FOUND) {
                throw ResourceNotFoundException(ZombieMapper.parseMissingIds(e.message))
            }
        }

        return emptyList<Store>()
    }

    override fun getBrand(id: Long): Optional<Brand> {
        val endpoint = "/rest/v1/zombie/brand/$id/"

        var brand = empty<Brand>()

        try {
            val response = restTemplate.getForObject(endpoint, Brand::class.java)
            brand = Optional.ofNullable(response)
        } catch (e: ClientException) {
            log.error("Error getting a brand [$id]. Details {}", e.message)
        }

        return brand
    }

}
