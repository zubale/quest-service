package com.zubale.quest.client.zombie.auth

import com.zubale.quest.domain.zombie.AuthToken
import java.util.Optional

interface ZombieAuthClient {

    fun retrieveToken(username: String, password: String): Optional<AuthToken>

}
