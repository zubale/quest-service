package com.zubale.quest.client

import com.zubale.quest.client.redemption.RedemptionClient
import com.zubale.quest.client.redemption.RedemptionClientImpl
import com.zubale.quest.client.zombie.auth.ZombieAuthClient
import com.zubale.quest.client.zombie.auth.ZombieAuthClientImpl
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class ClientFactory(
        private val zombieAuthClientRestTemplate: RestTemplate,
        private val redemptionAdminRestTemplate: RestTemplate
) {

    @Bean
    @Scope(BeanDefinition.SCOPE_SINGLETON)
    fun zombieAuthClient(): ZombieAuthClient = ZombieAuthClientImpl(zombieAuthClientRestTemplate)

    @Bean
    fun redemptionClient(): RedemptionClient = RedemptionClientImpl(redemptionAdminRestTemplate)

}
