package com.zubale.quest.client

import org.springframework.http.HttpStatus

class ClientException : RuntimeException {

    lateinit var statusCode: HttpStatus

    constructor(message: String?, ex: Exception?) : super(message, ex)

    constructor(message: String?, statusCode: HttpStatus) : super(message) {
        this.statusCode = statusCode
    }

    constructor(ex: Exception) : super(ex)
    constructor()

}
