package com.zubale.quest.client

import org.springframework.http.HttpStatus
import org.springframework.http.client.ClientHttpResponse
import org.springframework.stereotype.Component
import org.springframework.util.StreamUtils
import org.springframework.web.client.ResponseErrorHandler

@Component
class ClientErrorHandler : ResponseErrorHandler {

    override fun hasError(response: ClientHttpResponse): Boolean {
        return response.statusCode.series() == HttpStatus.Series.CLIENT_ERROR || response.statusCode.series() == HttpStatus.Series.SERVER_ERROR
    }

    override fun handleError(response: ClientHttpResponse) {
        if (response.statusCode.series() == HttpStatus.Series.SERVER_ERROR || response.statusCode.series() == HttpStatus.Series.CLIENT_ERROR) {
            val message = StreamUtils.copyToString(response.body, Charsets.UTF_8)
            throw ClientException(message, response.statusCode)
        }
    }

}
