package com.zubale.quest.repository

import com.zubale.quest.document.StoreDepartment
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.Locale

@Repository
interface StoreDepartmentRepository : MongoRepository<StoreDepartment, ObjectId> {

    fun findAllByLocale(locale: Locale): List<StoreDepartment>

}
