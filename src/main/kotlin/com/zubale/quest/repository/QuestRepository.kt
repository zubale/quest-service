package com.zubale.quest.repository

import com.zubale.quest.document.Quest
import org.bson.types.ObjectId
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Point
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface QuestRepository : MongoRepository<Quest, ObjectId> {

    fun findByLocationNear(point: Point, distance: Distance, pageable: Pageable): Page<Quest>

    fun findByClosedIsFalseAndCycle_StartDateBeforeAndCycle_EndDateAfterAndLocationNear(
            cycleStart: LocalDateTime, cycleEnd: LocalDateTime, point: Point, distance: Distance, pageable: Pageable): Page<Quest>

    fun findByBrandZubaleIdAndCycle_StartDateBetween(
            zubaleId: Long, fromDate: LocalDateTime, toDate: LocalDateTime): List<Quest>

}
