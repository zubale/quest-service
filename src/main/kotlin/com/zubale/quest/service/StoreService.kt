package com.zubale.quest.service

import com.zubale.quest.document.StoreDepartment
import com.zubale.quest.repository.StoreDepartmentRepository
import org.bson.types.ObjectId
import org.springframework.stereotype.Service
import java.util.Locale
import java.util.Optional

@Service
class StoreService(private val storeDepartmentRepository: StoreDepartmentRepository) {

    fun getDepartments(locale: Locale): List<StoreDepartment> = storeDepartmentRepository.findAllByLocale(locale)

    fun getDepartment(id: ObjectId): Optional<StoreDepartment> = storeDepartmentRepository.findById(id)

}
