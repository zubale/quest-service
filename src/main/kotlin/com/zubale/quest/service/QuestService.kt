package com.zubale.quest.service

import com.zubale.quest.configuration.properties.QuestProperties
import com.zubale.quest.document.Quest
import com.zubale.quest.domain.CacheNames
import com.zubale.quest.domain.QuestMapper.requestToQuest
import com.zubale.quest.domain.redemption.RewardRequest
import com.zubale.quest.exception.QuestCompletedException
import com.zubale.quest.exception.ResourceBadRequestException
import com.zubale.quest.exception.ResourceNotFoundException
import com.zubale.quest.repository.QuestRepository
import com.zubale.quest.representation.CompletionRepresentation
import com.zubale.quest.representation.CycleRepresentation
import com.zubale.quest.representation.enumerator.PromoterType
import com.zubale.quest.representation.filter.PageFilter
import com.zubale.quest.representation.filter.toPageRequest
import com.zubale.quest.representation.request.BrandRequest
import com.zubale.quest.representation.request.LocationRequest
import com.zubale.quest.representation.request.QuestRequest
import com.zubale.quest.util.toUTC
import mu.KotlinLogging
import org.bson.types.ObjectId
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.cache.annotation.Caching
import org.springframework.data.domain.Page
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Metrics
import org.springframework.data.geo.Point
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.Optional
import java.util.function.Consumer

@Service
class QuestService(
        private val questProperties: QuestProperties,
        private val questRepository: QuestRepository,
        private val zombieService: ZombieService,
        private val storeService: StoreService,
        private val redemptionService: RedemptionService
) {

    private val log = KotlinLogging.logger {}

    @Cacheable(cacheNames = [CacheNames.QUEST], key = "#id")
    fun findBy(id: ObjectId): Optional<Quest> = questRepository.findById(id)

    @Cacheable(cacheNames = [CacheNames.QUEST_LOCATIONS], key = "#locationRequest.latitude.toString().concat('-').concat(#locationRequest.longitude)")
    fun findBy(locationRequest: LocationRequest, pageFilter: PageFilter): Page<Quest> {
        val locationDistance = locationRequest.distance ?: questProperties.distance
        val point = Point(locationRequest.longitude, locationRequest.latitude)
        val distance = Distance(locationDistance, Metrics.KILOMETERS)
        val now = LocalDateTime.now()

        return questRepository.findByClosedIsFalseAndCycle_StartDateBeforeAndCycle_EndDateAfterAndLocationNear(
                now, now, point, distance, pageFilter.toPageRequest())
    }

    @Cacheable(cacheNames = [CacheNames.QUEST_BRANDS_FILTER], key = "#brandRequest.zubaleId.toString().concat('-').concat(#brandRequest.fromDate).concat('-').concat(#brandRequest.toDate)")
    fun findByBrandFilter(brandRequest: BrandRequest): List<Quest> {
        val fromDate = LocalDateTime.parse(brandRequest.fromDate);
        val toDate = LocalDateTime.parse(brandRequest.toDate);
        return questRepository.findByBrandZubaleIdAndCycle_StartDateBetween(
                brandRequest.zubaleId, fromDate, toDate)
    }

    fun findBy(pageFilter: PageFilter): Page<Quest> = questRepository.findAll(pageFilter.toPageRequest())

    @CacheEvict(cacheNames = [CacheNames.QUEST_LOCATIONS], allEntries = true)
    fun create(request: QuestRequest) {
        val startDate = request.cycle.startDate.toUTC()
        val endDate = request.cycle.endDate.toUTC()

        if (startDate.isAfter(endDate)) throw ResourceBadRequestException("The start date should be less than the end date in a cycle")

        val questRequest = request.copy(cycle = CycleRepresentation(startDate, endDate))
        val brand = zombieService.getBrand(questRequest.brandId).orElseThrow { ResourceNotFoundException("Brand [${questRequest.brandId}] not found") }

        val stores = questRequest.stores
                .map { Pair(it.storeId, it.department) }
                .map { (storeId, departmentId) ->
                    val store = zombieService.getStore(storeId).orElseThrow { ResourceNotFoundException("Store [$storeId]] not found") }
                    val department = storeService.getDepartment(departmentId).orElseThrow { ResourceNotFoundException("Department [$departmentId] not found") }
                    Pair(store, department)
                }

        stores.forEach(Consumer { (store, department) ->
            val quest = requestToQuest(questRequest, brand, store, department)
            questRepository.save(quest)
        })
    }

    @Caching(evict = [
        CacheEvict(cacheNames = [CacheNames.QUEST_LOCATIONS], allEntries = true, beforeInvocation = true),
        CacheEvict(cacheNames = [CacheNames.QUEST], key = "#id", beforeInvocation = true)
    ])
    fun closeOrOpenQuest(id: ObjectId, close: Boolean) {
        val quest = questRepository.findById(id).orElseThrow { ResourceNotFoundException("Quest [$id] not found") }

        if (quest.closed && quest.completion != null) {
            throw QuestCompletedException("You cannot open a completed quest")
        }

        questRepository.save(quest.copy(closed = close))
    }

    @Caching(evict = [
        CacheEvict(cacheNames = [CacheNames.QUEST_LOCATIONS], allEntries = true, beforeInvocation = true),
        CacheEvict(cacheNames = [CacheNames.QUEST], key = "#id", beforeInvocation = true)
    ])
    fun completeQuest(id: ObjectId, completionRepresentation: CompletionRepresentation) {
        val quest = questRepository.findById(id).orElseThrow { ResourceNotFoundException("Quest [$id] not found") }

        if (!quest.closed) {
            throw QuestCompletedException("To complete the quest [$id], you need to close it first")
        }

        val questSaved = questRepository.save(quest.copy(closed = true, completion = completionRepresentation))

        if (questSaved.completion == null) {
            log.error("Error saving the quest [$id] with completion [$completionRepresentation]")
        } else {
            if (completionRepresentation.autoPayment && completionRepresentation.promoterType == PromoterType.PROMOTER) {
                val request = RewardRequest(questSaved.completion.bonusAmount, questSaved.id.toHexString())
                val user = questSaved.completion.userId

                log.info("Trying to create a reward [${questSaved.completion}] for quest [${questSaved.id}] ")

                val rollback = Runnable {
                    val rollbackQuest = questRepository.save(questSaved.copy(completion = null))
                    throw QuestCompletedException("""
                    Error creating a reward for redemption service with user [$user] and quest [${rollbackQuest.id}]
                """.trimIndent())
                }
                val logSuccess = Consumer<Boolean> { log.info("Quest [${questSaved.id}] successfully completed") }

                redemptionService.rewardQuest(user, request).ifPresentOrElse(logSuccess, rollback)
            } else {
                log.info("The automatic payment it's not applying for the quest [$id] and completion [$completionRepresentation]")
            }
        }
    }

}
