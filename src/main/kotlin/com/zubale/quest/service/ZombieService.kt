package com.zubale.quest.service

import com.zubale.quest.client.zombie.ZombieClientHelper
import com.zubale.quest.client.zombie.base.ZombieClient
import com.zubale.quest.domain.zombie.Brand
import com.zubale.quest.domain.zombie.Store
import org.springframework.stereotype.Service
import java.util.Optional

@Service
class ZombieService(private val zombieClientHelper: ZombieClientHelper) {

    private fun client(): ZombieClient = zombieClientHelper.instance()

    fun getStores(): List<Store> = client().getStores()

    fun getStore(id: Long): Optional<Store> = client().getStore(id)

    fun getBrand(id: Long): Optional<Brand> = client().getBrand(id)

    fun getStoresByZubaleIds(zubaleIds: List<Long>): List<Store> = client().getStoresByZubaleIds(zubaleIds)

}
