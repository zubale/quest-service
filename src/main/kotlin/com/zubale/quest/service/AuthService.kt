package com.zubale.quest.service

import com.zubale.quest.representation.TokenRepresentation
import com.zubale.quest.representation.enumerator.TokenType.BEARER
import com.zubale.quest.security.TokenProvider
import com.zubale.quest.util.SecurityUtil
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class AuthService(
        private val authenticationManager: AuthenticationManager,
        private val tokenProvider: TokenProvider
) {

    fun login(basicToken: String): TokenRepresentation {
        val (username, password) = SecurityUtil.decodeBasicAuth(basicToken)
        val authentication = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))
        SecurityContextHolder.getContext().authentication = authentication

        val token = tokenProvider.generateToken(authentication)

        return TokenRepresentation(BEARER, token)
    }

}
