package com.zubale.quest.service

import com.zubale.quest.client.redemption.RedemptionClient
import com.zubale.quest.domain.redemption.RewardRequest
import org.springframework.stereotype.Service
import java.util.Optional
import java.util.UUID

@Service
class RedemptionService(private val redemptionClient: RedemptionClient) {

    fun rewardQuest(userId: UUID, request: RewardRequest): Optional<Boolean> = redemptionClient.rewardQuest(userId, request)

}
