package com.zubale.quest.configuration

import com.zubale.quest.configuration.properties.SwaggerProperties.API_DESCRIPTION
import com.zubale.quest.configuration.properties.SwaggerProperties.API_INFO
import com.zubale.quest.configuration.properties.SwaggerProperties.API_VERSION
import com.zubale.quest.configuration.properties.SwaggerProperties.BASIC_REFERENCE
import com.zubale.quest.configuration.properties.SwaggerProperties.PROTECTED_ENDPOINTS_REGEX
import com.zubale.quest.configuration.properties.SwaggerProperties.SCAN_PACKAGE
import com.zubale.quest.configuration.properties.SwaggerProperties.TOKEN_PARAMETER
import com.zubale.quest.configuration.properties.SwaggerProperties.TOKEN_REFERENCE
import com.zubale.quest.configuration.properties.SwaggerProperties.TOKEN_SCOPE
import com.zubale.quest.configuration.properties.SwaggerProperties.TOKEN_SCOPE_DESCRIPTION
import com.zubale.quest.security.SecurityConstants.AUTHORIZATION_HEADER
import com.zubale.quest.security.SecurityConstants.AUTH_LOGIN_URL
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.ApiKey
import springfox.documentation.service.AuthorizationScope
import springfox.documentation.service.BasicAuth
import springfox.documentation.service.SecurityReference
import springfox.documentation.service.SecurityScheme
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spi.service.contexts.SecurityContext
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage(SCAN_PACKAGE))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .securitySchemes(securitySchemes())
                .securityContexts(securityContext())
    }

    private fun apiInfo(): ApiInfo = ApiInfo(API_INFO, API_DESCRIPTION, API_VERSION, null, null, null, null, emptyList())

    private fun securitySchemes(): List<SecurityScheme> = listOf(
            BasicAuth(BASIC_REFERENCE),
            ApiKey(TOKEN_REFERENCE, AUTHORIZATION_HEADER, TOKEN_PARAMETER)
    )

    private fun securityContext(): List<SecurityContext> = listOf(
            SecurityContext.builder()
                    .securityReferences(basicAuth())
                    .forPaths(PathSelectors.ant(AUTH_LOGIN_URL))
                    .build(),
            SecurityContext.builder()
                    .securityReferences(bearerAuth())
                    .forPaths(PathSelectors.regex(PROTECTED_ENDPOINTS_REGEX))
                    .build()
    )

    private fun basicAuth(): List<SecurityReference> = listOf(SecurityReference(BASIC_REFERENCE, generalScope()))

    private fun bearerAuth(): List<SecurityReference> = listOf(SecurityReference(TOKEN_REFERENCE, generalScope()))

    private fun generalScope(): Array<AuthorizationScope> = arrayOf(AuthorizationScope(TOKEN_SCOPE, TOKEN_SCOPE_DESCRIPTION))

}
