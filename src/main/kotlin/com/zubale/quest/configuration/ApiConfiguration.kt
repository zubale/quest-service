package com.zubale.quest.configuration

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.time.ZoneOffset
import java.util.TimeZone
import javax.annotation.PostConstruct

@Configuration
class ApiConfiguration : WebMvcConfigurer {

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH")
    }

    @PostConstruct
    fun defaultTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC))
    }

}
