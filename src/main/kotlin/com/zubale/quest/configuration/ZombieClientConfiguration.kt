package com.zubale.quest.configuration

import com.zubale.quest.client.ClientErrorHandler
import com.zubale.quest.configuration.properties.ZombieClientProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.client.RestTemplate

@EnableScheduling
@Configuration
class ZombieClientConfiguration(private val zombieClientProperties: ZombieClientProperties) {

    @Bean
    fun zombieAuthClientHttpFactory(): HttpComponentsClientHttpRequestFactory = HttpComponentsClientHttpRequestFactory()

    @Bean
    @ConfigurationProperties(prefix = "service.zombie.connection")
    fun zombieAuthClientRestTemplate(builder: RestTemplateBuilder, zombieAuthClientHttpFactory: ClientHttpRequestFactory): RestTemplate {
        return builder
                .requestFactory(zombieAuthClientHttpFactory::class.java)
                .rootUri(zombieClientProperties.baseURL)
                .basicAuthentication(zombieClientProperties.username, zombieClientProperties.password)
                .errorHandler(ClientErrorHandler())
                .build()
    }

}
