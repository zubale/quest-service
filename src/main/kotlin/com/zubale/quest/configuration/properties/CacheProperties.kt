package com.zubale.quest.configuration.properties

import com.zubale.quest.domain.CacheExpiration
import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "cache")
class CacheProperties {

    lateinit var redisUri: String
    var defaultExpiration: CacheExpiration = CacheExpiration()
    var cacheExpirations: HashMap<String, CacheExpiration> = HashMap()

}
