package com.zubale.quest.configuration.properties

object SwaggerProperties {

    const val SCAN_PACKAGE = "com.zubale.quest.controller"
    const val PROTECTED_ENDPOINTS_REGEX = "((?!\\/auth\\/login).)*$"

    const val API_INFO = "Zubale"
    const val API_DESCRIPTION = "Quest service API"
    const val API_VERSION = "1.0"

    const val BASIC_REFERENCE = "Basic authorization"
    const val TOKEN_REFERENCE = "(token_type) (token)"
    const val TOKEN_PARAMETER = "header"
    const val TOKEN_SCOPE = "global"
    const val TOKEN_SCOPE_DESCRIPTION = "JWT access token"

}
