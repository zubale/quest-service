package com.zubale.quest.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "service.redemption")
class RedemptionClientProperties {

    lateinit var adminUsername: String
    lateinit var adminPassword: String
    lateinit var baseURL: String

}
