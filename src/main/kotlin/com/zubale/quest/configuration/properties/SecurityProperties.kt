package com.zubale.quest.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "service.auth")
class SecurityProperties {

    lateinit var userName: String
    lateinit var userPassword: String
    lateinit var adminName: String
    lateinit var adminPassword: String
    lateinit var secret: String

}
