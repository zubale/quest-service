package com.zubale.quest.configuration.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "service.zombie")
class ZombieClientProperties {

    lateinit var username: String
    lateinit var password: String
    lateinit var baseURL: String

}
