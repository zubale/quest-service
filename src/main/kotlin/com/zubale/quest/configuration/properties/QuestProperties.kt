package com.zubale.quest.configuration.properties

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
data class QuestProperties(

        @Value("\${service.distance}")
        val distance: Double

)
