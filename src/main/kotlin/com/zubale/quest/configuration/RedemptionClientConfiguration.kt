package com.zubale.quest.configuration

import com.zubale.quest.client.ClientErrorHandler
import com.zubale.quest.configuration.properties.RedemptionClientProperties
import mu.KotlinLogging
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

@Configuration
class RedemptionClientConfiguration (private val redemptionClientProperties: RedemptionClientProperties) {

    private val log = KotlinLogging.logger {}

    @Bean
    fun redemptionClientHttpFactory(): HttpComponentsClientHttpRequestFactory = HttpComponentsClientHttpRequestFactory()

    @Bean
    @ConfigurationProperties(prefix = "service.redemption.connection")
    fun redemptionAdminRestTemplate(builder: RestTemplateBuilder, redemptionClientHttpFactory: ClientHttpRequestFactory): RestTemplate {
        return builder
                .requestFactory(redemptionClientHttpFactory::class.java)
                .rootUri(redemptionClientProperties.baseURL)
                .basicAuthentication(redemptionClientProperties.adminUsername, redemptionClientProperties.adminPassword)
                .errorHandler(ClientErrorHandler())
                .build()
    }

}
