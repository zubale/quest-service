package com.zubale.quest.configuration

import com.zubale.quest.configuration.properties.CacheProperties
import com.zubale.quest.domain.CacheExpiration
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.CachingConfigurerSupport
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.redis.cache.RedisCacheConfiguration
import org.springframework.data.redis.cache.RedisCacheManager
import org.springframework.data.redis.connection.RedisConnectionFactory
import org.springframework.data.redis.connection.RedisPassword
import org.springframework.data.redis.connection.RedisStandaloneConfiguration
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer
import org.springframework.data.redis.serializer.StringRedisSerializer
import java.net.URI
import java.time.Duration

@EnableConfigurationProperties(CacheProperties::class)
@Configuration
@EnableCaching
class CacheConfiguration : CachingConfigurerSupport() {

    fun createCacheConfiguration(expiration: CacheExpiration): RedisCacheConfiguration {
        return RedisCacheConfiguration
                .defaultCacheConfig()
                .entryTtl(Duration.of(expiration.ttlTimeout.toLong(), expiration.durationUnit))
    }

    fun cacheConfig(properties: CacheProperties): RedisCacheConfiguration {
        val default = CacheExpiration()
        default.durationUnit = properties.defaultExpiration.durationUnit
        default.ttlTimeout = properties.defaultExpiration.ttlTimeout

        return createCacheConfiguration(default)
    }

    @Bean
    fun redisConnectionFactory(properties: CacheProperties): LettuceConnectionFactory {
        val uri = URI(properties.redisUri)
        val redisConfiguration = RedisStandaloneConfiguration()
        redisConfiguration.hostName = uri.host
        redisConfiguration.port = uri.port
        redisConfiguration.password = RedisPassword.of(uri.userInfo.split(":")[1])

        return LettuceConnectionFactory(redisConfiguration)
    }

    @Primary
    @Bean
    fun redisTemplate(connectionFactory: RedisConnectionFactory): RedisTemplate<String, Any> {
        val redisTemplate = RedisTemplate<String, Any>()
        redisTemplate.connectionFactory = connectionFactory
        redisTemplate.keySerializer = StringRedisSerializer()
        redisTemplate.hashKeySerializer = StringRedisSerializer()
        redisTemplate.valueSerializer = GenericJackson2JsonRedisSerializer()
        redisTemplate.hashValueSerializer = GenericJackson2JsonRedisSerializer()
        redisTemplate.setEnableTransactionSupport(true)

        return redisTemplate
    }

    @Bean
    fun cacheManager(redisConnectionFactory: RedisConnectionFactory, properties: CacheProperties): CacheManager {
        val cacheConfigurations = HashMap<String, RedisCacheConfiguration>()

        for (cacheNamed in properties.cacheExpirations.entries) {
            cacheConfigurations[cacheNamed.key] = createCacheConfiguration(cacheNamed.value)
        }

        return RedisCacheManager.builder(redisConnectionFactory)
                .cacheDefaults(cacheConfig(properties))
                .withInitialCacheConfigurations(cacheConfigurations)
                .build()
    }

}
