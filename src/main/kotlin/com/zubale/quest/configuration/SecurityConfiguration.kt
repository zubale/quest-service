package com.zubale.quest.configuration

import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.security.AuthorizationFilter
import com.zubale.quest.security.ForbiddenEntryPoint
import com.zubale.quest.security.SecurityConstants.AUTH_LOGIN_URL
import com.zubale.quest.security.TokenProvider
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
class SecurityConfiguration(
        private val securityProperties: SecurityProperties,
        private val unauthorizedHandler: ForbiddenEntryPoint,
        private val tokenProvider: TokenProvider
) : WebSecurityConfigurerAdapter() {

    private val whiteList = arrayOf(
            // actuator
            "/_healthcheck/**",
            "/info/**",
            // swagger
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**",
            // public services
            AUTH_LOGIN_URL
    )

    override fun configure(web: WebSecurity) {
        web.ignoring().antMatchers(*whiteList)
    }

    override fun configure(http: HttpSecurity) {
        http.cors()
                .and()
                .csrf().disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(*whiteList).permitAll()
                .antMatchers("/public/**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/private/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .addFilter(AuthorizationFilter(tokenProvider, authenticationManager()))
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
                .withUser(securityProperties.userName)
                .password(passwordEncoder().encode(securityProperties.userPassword))
                .roles("USER")
                .and()
                .withUser(securityProperties.adminName)
                .password(passwordEncoder().encode(securityProperties.adminPassword))
                .roles("ADMIN")
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues())

        return source
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

}
