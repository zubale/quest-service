package com.zubale.quest.document

import com.zubale.quest.representation.BrandRepresentation
import com.zubale.quest.representation.CompletionRepresentation
import com.zubale.quest.representation.CycleRepresentation
import com.zubale.quest.representation.GoogleFormRepresentation
import com.zubale.quest.representation.StoreDepartmentRepresentation
import com.zubale.quest.representation.StoreRepresentation
import com.zubale.quest.representation.enumerator.QuestType
import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDateTime

@Document(collection = "quests")
data class Quest(
        @Id
        val id: ObjectId = ObjectId(),

        @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
        val location: GeoJsonPoint,

        val name: String,
        val description: String,
        val type: QuestType,
        val rewardAmount: BigDecimal,
        val duration: String,
        val cycle: CycleRepresentation,
        val googleForm: GoogleFormRepresentation,
        val brand: BrandRepresentation,
        val department: StoreDepartmentRepresentation,
        val store: StoreRepresentation,
        val country: String,
        val closed: Boolean = false,
        val completion: CompletionRepresentation? = null,

        @CreatedDate
        val createdAt: LocalDateTime = LocalDateTime.now(),

        @LastModifiedDate
        val updatedAt: LocalDateTime = LocalDateTime.now()
) : Serializable
