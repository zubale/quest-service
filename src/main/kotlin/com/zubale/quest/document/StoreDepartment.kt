package com.zubale.quest.document

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.CompoundIndex
import org.springframework.data.mongodb.core.mapping.Document
import java.io.Serializable
import java.util.Locale

@Document("storeDepartments")
@CompoundIndex(def = "{'name': 1, 'locale': 1}", unique = true)
data class StoreDepartment(

        @Id
        val id: ObjectId = ObjectId(),

        val name: String,
        val locale: Locale

) : Serializable
