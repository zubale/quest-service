package com.zubale.quest.security

object SecurityConstants {

    const val AUTH_LOGIN_URL = "/auth/login"

    // JWT token props
    const val AUTHORIZATION_HEADER = "Authorization"
    const val TOKEN_BEARER = "Bearer "
    const val TOKEN_BASIC = "Basic "
    const val TOKEN_TYPE = "JWT"
    const val TOKEN_ISSUER = "secure-api"
    const val TOKEN_AUDIENCE = "secure-app"

    const val TOKEN_EXPIRATION_WEEK = 604800000

}
