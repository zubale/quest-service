package com.zubale.quest.security

import com.zubale.quest.security.SecurityConstants.AUTHORIZATION_HEADER
import com.zubale.quest.security.SecurityConstants.TOKEN_BEARER
import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.StringUtils.EMPTY
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.stream.Collectors.toList
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthorizationFilter(provider: TokenProvider, authenticationManager: AuthenticationManager) : BasicAuthenticationFilter(authenticationManager) {

    private val tokenProvider: TokenProvider = provider

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val authentication = getAuthentication(request)

        if (authentication == null) {
            chain.doFilter(request, response)
            return
        }

        SecurityContextHolder.getContext().authentication = authentication
        chain.doFilter(request, response)
    }

    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token = getTokenFromRequest(request)

        if (StringUtils.isNotBlank(token) && tokenProvider.isValidToken(token)) {
            val username = tokenProvider.getUserFromToken(token!!)
            val roles = tokenProvider.getAllClaims(token)["rol"] as List<String>
            val authorities = roles.stream().map(::SimpleGrantedAuthority).collect(toList())

            if (StringUtils.isNotEmpty(username)) {
                return UsernamePasswordAuthenticationToken(username, null, authorities)
            }
        }

        return null
    }

    private fun getTokenFromRequest(request: HttpServletRequest): String? {
        val bearerToken = request.getHeader(AUTHORIZATION_HEADER)

        if (StringUtils.isNoneBlank(bearerToken) && bearerToken.startsWith(TOKEN_BEARER)) {
            return bearerToken.replace(TOKEN_BEARER, EMPTY).trim()
        }

        return null
    }

}
