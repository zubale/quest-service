package com.zubale.quest.security

import com.zubale.quest.domain.ErrorDetails
import com.zubale.quest.util.JsonUtil
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component
import java.nio.charset.StandardCharsets
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class ForbiddenEntryPoint : AuthenticationEntryPoint {

    private val log = KotlinLogging.logger {}

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, authException: AuthenticationException) {
        log.error("Responding with forbidden error. Message: {}", authException.message)

        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = MediaType.APPLICATION_JSON_UTF8_VALUE
        response.characterEncoding = StandardCharsets.UTF_8.name()

        val error = ErrorDetails(message = authException.message, path = request.requestURI)
        val jsonError = JsonUtil.toJson(error)
        val writer = response.writer

        writer.print(jsonError)
        writer.flush()
    }

}
