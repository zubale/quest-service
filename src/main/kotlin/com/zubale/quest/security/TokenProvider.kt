package com.zubale.quest.security

import com.zubale.quest.configuration.properties.SecurityProperties
import com.zubale.quest.security.SecurityConstants.TOKEN_AUDIENCE
import com.zubale.quest.security.SecurityConstants.TOKEN_EXPIRATION_WEEK
import com.zubale.quest.security.SecurityConstants.TOKEN_ISSUER
import com.zubale.quest.security.SecurityConstants.TOKEN_TYPE
import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jws
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.UnsupportedJwtException
import io.jsonwebtoken.security.Keys
import io.jsonwebtoken.security.SignatureException
import mu.KotlinLogging
import org.apache.commons.lang3.StringUtils
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component
import java.util.Date
import java.util.stream.Collectors

@Component
class TokenProvider(private val securityProperties: SecurityProperties) {

    private val log = KotlinLogging.logger {}

    fun generateToken(authentication: Authentication): String {
        val user = authentication.principal as User

        val roles = user.authorities.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList())

        val now = Date()
        val expiryDate = Date(now.time + TOKEN_EXPIRATION_WEEK)

        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(getSigningKey()), SignatureAlgorithm.HS512)
                .setHeaderParam("typ", TOKEN_TYPE)
                .setIssuer(TOKEN_ISSUER)
                .setIssuedAt(now)
                .setAudience(TOKEN_AUDIENCE)
                .setSubject(user.username)
                .claim("rol", roles)
                .setExpiration(expiryDate)
                .compact()
    }

    fun getUserFromToken(token: String): String = getClaim(token, Claims::getSubject)

    fun <T> getClaim(token: String, resolver: (Claims) -> T): T {
        val claims = getAllClaims(token)
        return resolver(claims)
    }

    fun getAllClaims(token: String): Claims = parseToken(token).body

    fun isValidToken(token: String?): Boolean {
        if (StringUtils.isBlank(token)) return false

        try {
            parseToken(token!!)
            return true
        } catch (exception: ExpiredJwtException) {
            log.warn("Request to parse expired JWT failed: {}", exception.message)
        } catch (exception: UnsupportedJwtException) {
            log.warn("Request to parse unsupported JWT failed: {}", exception.message)
        } catch (exception: MalformedJwtException) {
            log.warn("Request to parse invalid JWT  failed: {}", exception.message)
        } catch (exception: SignatureException) {
            log.warn("Request to parse JWT with invalid signature failed: {}", exception.message)
        } catch (exception: IllegalArgumentException) {
            log.warn("Request to parse empty or null JWT failed: {}", exception.message)
        }

        return false
    }

    private fun parseToken(token: String): Jws<Claims> {
        return Jwts.parser()
                .setSigningKey(getSigningKey())
                .parseClaimsJws(token)
    }

    private fun getSigningKey(): ByteArray = securityProperties.secret.toByteArray(Charsets.UTF_8)

}
