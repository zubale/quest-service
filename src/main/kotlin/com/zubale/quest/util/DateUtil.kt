package com.zubale.quest.util

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset

fun LocalDateTime.toUTC(): LocalDateTime {
    val dateTime = DateTime(this.toString()).toDateTime(DateTimeZone.UTC)
    val instant = Instant.ofEpochMilli(dateTime.millis)

    return LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
}
