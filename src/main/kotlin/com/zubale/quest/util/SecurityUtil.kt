package com.zubale.quest.util

import com.zubale.quest.security.SecurityConstants.TOKEN_BASIC
import org.apache.commons.lang3.StringUtils
import java.util.Base64

object SecurityUtil {

    fun encodeBasicAuth(username: String, password: String): String {
        val builder = StringBuilder(username)
                .append(":")
                .append(password)

        val token = builder.toString()

        return Base64.getEncoder()
                .encode(token.toByteArray(Charsets.UTF_8))
                .toString(Charsets.UTF_8)
    }

    fun decodeBasicAuth(basicToken: String): List<String> {
        var basic = basicToken

        if (StringUtils.isBlank(basicToken) || !basicToken.startsWith(TOKEN_BASIC)) {
            basic = StringUtils.EMPTY
        }

        var decode = Base64.getDecoder()
                .decode(basic.replace(TOKEN_BASIC, StringUtils.EMPTY))
                .toString(Charsets.UTF_8).trim().split(":")

        if (decode.size < 2) {
            decode = listOf(StringUtils.EMPTY, StringUtils.EMPTY)
        }

        return decode
    }

}
