package com.zubale.quest.util

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule

object JsonUtil {

    fun <R> toJsonNode(representation: R): JsonNode = createMapper().valueToTree(representation)

    fun jsonStringToNode(json: String): JsonNode = createMapper().readTree(json)

    fun <R> toJson(representation: R): String = toJson(createMapper().writer(), representation)

    fun <R> toJson(writer: ObjectWriter, representation: R): String = writer.writeValueAsString(representation)

    fun createMapper(): ObjectMapper = ObjectMapper()
            .registerModule(KotlinModule())
            .registerModule(Jdk8Module())
            .registerModule(JavaTimeModule())
            .enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL)

    inline fun <reified T : Any> fromJson(json: String): T = createMapper().readValue(json, T::class.java)

    inline fun <reified T : Any> fromNode(jsonNode: JsonNode): T = createMapper().treeToValue(jsonNode, T::class.java)

    inline fun <reified T : Any> ObjectMapper.readValue(content: String): T = readValue(content, object : TypeReference<T>() {})

    inline fun <reified T : Any> fromJsonToList(json: String): List<T> = createMapper().readValue(json)

    fun isValidJson(json: String): Boolean {
        var valid = true

        try {
            createMapper().readTree(json)
        } catch (e: JsonProcessingException) {
            valid = false
        }

        return valid
    }

}
