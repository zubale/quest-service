package com.zubale.quest.util

import com.zubale.quest.domain.Location
import org.springframework.data.geo.Metrics
import java.lang.Math.toDegrees
import java.lang.Math.toRadians
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin

object DistanceHelper {

    const val BASE_EARTH_DISTANCE = 60.0
    const val MILES_FACTOR = 1.1515
    const val NEUTRAL_FACTOR = 0.8684
    const val KILOMETERS_FACTOR = 1.609344

    fun distance(origin: Location, destination: Location, unit: Metrics): Double {
        if (origin.latitude == destination.latitude && origin.longitude == origin.longitude) {
            return 0.0
        } else {
            val theta = origin.longitude - destination.longitude
            var dist = sin(toRadians(origin.latitude)) * sin(toRadians(destination.latitude)) + cos(toRadians(origin.latitude)) * cos(toRadians(destination.latitude)) * cos(toRadians(theta))
            dist = acos(dist)
            dist = toDegrees(dist)

            dist *= BASE_EARTH_DISTANCE * MILES_FACTOR // Miles result

            return when (unit) {
                Metrics.NEUTRAL -> dist * NEUTRAL_FACTOR
                Metrics.KILOMETERS -> dist * KILOMETERS_FACTOR
                else -> dist
            }
        }
    }

}
