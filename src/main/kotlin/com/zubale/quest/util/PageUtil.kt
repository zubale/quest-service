package com.zubale.quest.util

import com.zubale.quest.representation.PageRepresentation
import org.springframework.data.domain.Page

object PageUtil {

    const val DEFAULT_PAGE = 0
    const val DEFAULT_PAGE_SIZE = 10

    fun <T> toRepresentation(page: Page<T>): PageRepresentation<T> {
        return PageRepresentation(
                content = page.content.takeUnless { it.isEmpty() } ?: emptyList(),
                page = page.number,
                pageSize = page.size,
                totalPages = page.totalPages,
                totalSize = page.totalElements
        )
    }

}
