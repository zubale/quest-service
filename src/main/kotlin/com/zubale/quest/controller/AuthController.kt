package com.zubale.quest.controller

import com.zubale.quest.representation.TokenRepresentation
import com.zubale.quest.service.AuthService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.annotations.ApiIgnore

@Api(description = "Auth related endpoints")
@RestController
@RequestMapping("/auth")
class AuthController(private val authService: AuthService) {

    @ApiOperation("Login into the service")
    @PostMapping("/login")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun login(@ApiIgnore @RequestHeader(name = HttpHeaders.AUTHORIZATION) basicToken: String): TokenRepresentation {
        return authService.login(basicToken)
    }

}
