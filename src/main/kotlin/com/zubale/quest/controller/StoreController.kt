package com.zubale.quest.controller

import com.zubale.quest.domain.ZombieMapper.storeDepartmentToRepresentation
import com.zubale.quest.representation.StoreDepartmentRepresentation
import com.zubale.quest.service.StoreService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.Locale

@Api(description = "Store public related endpoints")
@RestController
@RequestMapping("/public/v1/stores")
class StoreController(private val storeService: StoreService) {

    @ApiOperation("Get catalogue from store departments")
    @GetMapping("/departments")
    fun getDepartments(): List<StoreDepartmentRepresentation> {
        // TODO change this one for the future with the accept-language header
        val mexicoLocale = Locale("es", "MX")

        return storeService.getDepartments(mexicoLocale)
                .map { storeDepartmentToRepresentation(it) }
    }

}
