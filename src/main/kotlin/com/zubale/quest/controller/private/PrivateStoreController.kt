package com.zubale.quest.controller.private

import com.zubale.quest.domain.ZombieMapper.storeToRepresentation
import com.zubale.quest.representation.StoreRepresentation
import com.zubale.quest.service.ZombieService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(description = "Store private related endpoints")
@RestController
@RequestMapping("/private/v1/stores")
class PrivateStoreController(private val zombieService: ZombieService) {

    @ApiOperation("Get bulk of stores by zubale ids")
    @PostMapping("/bulk/zubale-identifier")
    fun getDepartments(@RequestBody zubaleIds: List<Long>): List<StoreRepresentation> {
        return zombieService.getStoresByZubaleIds(zubaleIds)
                .map { storeToRepresentation(it) }
    }

}
