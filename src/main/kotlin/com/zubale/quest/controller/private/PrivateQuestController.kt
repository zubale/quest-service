package com.zubale.quest.controller.private

import com.zubale.quest.representation.CompletionRepresentation
import com.zubale.quest.representation.request.QuestRequest
import com.zubale.quest.service.QuestService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.bson.types.ObjectId
import org.springframework.http.HttpStatus.ACCEPTED
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.HttpStatus.NO_CONTENT
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Api(description = "Quest private related endpoints")
@RestController
@RequestMapping("/private/v1/quests")
class PrivateQuestController(private val questService: QuestService) {

    @ApiOperation("Create a quest")
    @PostMapping
    @ResponseStatus(CREATED)
    fun create(@Valid @RequestBody questRequest: QuestRequest) = questService.create(questRequest)

    @ApiOperation("Close a quest")
    @PatchMapping("/{id}/close")
    @ResponseStatus(NO_CONTENT)
    fun closeQuest(@PathVariable id: ObjectId) = questService.closeOrOpenQuest(id, true)

    @ApiOperation("Open a quest")
    @PatchMapping("/{id}/open")
    @ResponseStatus(NO_CONTENT)
    fun openQuest(@PathVariable id: ObjectId) = questService.closeOrOpenQuest(id, false)

    @ApiOperation("Complete a quest")
    @PostMapping("/{id}/complete")
    @ResponseStatus(ACCEPTED)
    fun completeQuest(
            @PathVariable id: ObjectId,
            @Valid @RequestBody completionRepresentation: CompletionRepresentation
    ) = questService.completeQuest(id, completionRepresentation)

}
