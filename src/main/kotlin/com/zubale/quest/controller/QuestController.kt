package com.zubale.quest.controller

import com.zubale.quest.domain.Location
import com.zubale.quest.domain.QuestMapper.toRepresentation
import com.zubale.quest.exception.ResourceNotFoundException
import com.zubale.quest.representation.PageRepresentation
import com.zubale.quest.representation.QuestRepresentation
import com.zubale.quest.representation.filter.PageFilter
import com.zubale.quest.representation.filter.toPageRequest
import com.zubale.quest.representation.request.BrandRequest
import com.zubale.quest.representation.request.LocationRequest
import com.zubale.quest.service.QuestService
import com.zubale.quest.util.DistanceHelper
import com.zubale.quest.util.PageUtil
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.bson.types.ObjectId
import org.springframework.data.domain.PageImpl
import org.springframework.data.geo.Metrics.KILOMETERS
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@Api(description = "Quest public related endpoints")
@RestController
@RequestMapping("/public/v1/quests")
class QuestController(private val questService: QuestService) {

    @ApiOperation("Get all quests")
    @GetMapping
    fun getQuests(pageFilter: PageFilter): PageRepresentation<QuestRepresentation> {
        val questPage = questService.findBy(pageFilter)

        val content = questPage.content.map { toRepresentation(it) }
        val pageable = pageFilter.toPageRequest()
        val page = PageImpl(content, pageable, questPage.totalElements)

        return PageUtil.toRepresentation(page)
    }

    @ApiOperation("Get all quest by location")
    @GetMapping("/location")
    fun getQuestsByLocation(@Valid locationRequest: LocationRequest, pageFilter: PageFilter): PageRepresentation<QuestRepresentation> {
        val questPage = questService.findBy(locationRequest, pageFilter)

        val content = questPage.content
                .map { toRepresentation(it) }
                .map { representation ->
                    val origin = Location(locationRequest.latitude, locationRequest.longitude)
                    val destination = Location(representation.location.latitude, representation.location.longitude)
                    val distance = DistanceHelper.distance(origin, destination, KILOMETERS)
                    representation.copy(distance = distance)
                }
        val pageable = pageFilter.toPageRequest()
        val page = PageImpl(content, pageable, questPage.totalElements)

        return PageUtil.toRepresentation(page)
    }

    @ApiOperation("Get a quest")
    @GetMapping("/{id}")
    fun get(@PathVariable id: ObjectId): QuestRepresentation {
        val quest = questService.findBy(id).orElseThrow(::ResourceNotFoundException)
        return toRepresentation(quest)
    }

    @ApiOperation("Get all zubale store Id's by brand and filtered by a range of cycle start date",
            notes = "Parameters:\n" +
            "zubaleId, the brand zubale ID \n" +
            "fromDate, the first date range for the cycle start date \n" +
            "toDate, the second date range for the cycle start date \n")
    @GetMapping("/brands-filter")
    fun getByBrandFilter(@Valid brandRequest: BrandRequest): List<Long> {
        val quests = questService.findByBrandFilter(brandRequest)
        return quests.map { it -> it.store.zubaleId }
    }

}
