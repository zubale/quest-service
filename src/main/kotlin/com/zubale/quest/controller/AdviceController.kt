package com.zubale.quest.controller

import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import com.zubale.quest.domain.ErrorDetails
import com.zubale.quest.exception.QuestCompletedException
import com.zubale.quest.exception.ResourceBadRequestException
import com.zubale.quest.exception.ResourceNotFoundException
import mu.KotlinLogging
import org.springframework.beans.BeanInstantiationException
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.FORBIDDEN
import org.springframework.http.HttpStatus.NOT_ACCEPTABLE
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE
import org.springframework.http.converter.HttpMessageConversionException
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.validation.BindException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.MissingRequestHeaderException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.StringBuilder
import java.time.format.DateTimeParseException
import java.util.Optional.ofNullable
import java.util.function.Consumer
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.reflect.jvm.javaType

@RestControllerAdvice
class AdviceController {

    private val log = KotlinLogging.logger {}

    @ResponseStatus(FORBIDDEN)
    @ExceptionHandler(BadCredentialsException::class)
    fun handleBadCredentialsException(exception: BadCredentialsException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        return ErrorDetails(
                message = buildMessage(exception, FORBIDDEN),
                path = request.requestURI
        )
    }

    @ResponseStatus(NOT_ACCEPTABLE)
    @ExceptionHandler(QuestCompletedException::class)
    fun handleQuestCompleteException(exception: QuestCompletedException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        return ErrorDetails(
                message = buildMessage(exception, BAD_REQUEST),
                path = request.requestURI
        )
    }

    @ResponseStatus(NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException::class)
    fun handleNotFoundException(exception: ResourceNotFoundException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        return ErrorDetails(
                message = buildMessage(exception, NOT_FOUND),
                path = request.requestURI
        )
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(BindException::class)
    fun handleBindException(exception: BindException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        val bindResult = exception.bindingResult
        val errorMessages = arrayListOf<String>()

        bindResult.fieldErrors.forEach(Consumer {
            errorMessages.add("${it.field} ${it.defaultMessage}")
        })

        val errorDetails = ErrorDetails(path = request.requestURI)

        return if (errorMessages.isEmpty()) {
            errorDetails.copy(message = BAD_REQUEST.reasonPhrase)
        } else {
            errorDetails.copy(messages = errorMessages)
        }
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(value = [HttpMessageNotReadableException::class, HttpMessageConversionException::class])
    fun handleMessageConversionException(exception: HttpMessageConversionException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        val mostSpecificCause = exception.mostSpecificCause

        val message = when (mostSpecificCause) {
            is IllegalArgumentException -> mostSpecificCause.message
            is MissingKotlinParameterException -> {
                val parameter = mostSpecificCause.parameter
                "Parameter [${parameter.name}] must not be null or the value is not valid, try to use a correct ${parameter.type.javaType.typeName.javaClass.simpleName}"
            }
            is InvalidFormatException -> {
                "${mostSpecificCause.value} is not a valid representation for ${mostSpecificCause.targetType.simpleName}"
            }
            is MismatchedInputException -> {
                var fieldName: String = mostSpecificCause.pathReference

                for (reference in mostSpecificCause.path) {
                    if (reference.fieldName != null) {
                        fieldName = reference.fieldName
                    }
                }

                "$fieldName is not a valid representation for ${mostSpecificCause.targetType.simpleName}"
            }
            else -> buildMessage(exception, BAD_REQUEST)
        }

        return ErrorDetails(
                message = message,
                path = request.requestURI
        )
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentException(exception: MethodArgumentNotValidException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        val fieldErrors = exception.bindingResult.fieldErrors
        val globalErrors = exception.bindingResult.globalErrors

        val size = fieldErrors.size + globalErrors.size
        val errors = ArrayList<String>(size)

        for (fieldError in fieldErrors) {
            val error = fieldError.field + ", " + fieldError.defaultMessage
            errors.add(error)
        }

        for (objectError in globalErrors) {
            val error = objectError.objectName + ", " + objectError.defaultMessage
            errors.add(error)
        }

        return ErrorDetails(
                messages = errors,
                path = request.requestURI
        )
    }

    @ResponseStatus(BAD_REQUEST)
    @ExceptionHandler(value = [
        BeanInstantiationException::class,
        DateTimeParseException::class,
        IllegalArgumentException::class,
        ResourceBadRequestException::class
    ])
    fun handleBadRequestException(exception: RuntimeException, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        return ErrorDetails(
                message = buildMessage(exception, BAD_REQUEST),
                path = request.requestURI
        )
    }

    @ResponseStatus(NOT_ACCEPTABLE)
    @ExceptionHandler(MissingRequestHeaderException::class)
    fun handleMissingRequestHeaderException(exception: Exception, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        return ErrorDetails(
                message = buildMessage(exception, NOT_ACCEPTABLE),
                path = request.requestURI
        )
    }

    @ResponseStatus(SERVICE_UNAVAILABLE)
    @ExceptionHandler(Exception::class)
    fun handleGeneralException(exception: Exception, request: HttpServletRequest, response: HttpServletResponse): ErrorDetails {
        log.error("Request: {}", request)
        log.error("Details: {}", exception)

        return ErrorDetails(
                message = SERVICE_UNAVAILABLE.reasonPhrase,
                path = request.requestURI
        )
    }

    private fun buildMessage(exception: Exception, status: HttpStatus): String {
        val builder = StringBuilder(status.reasonPhrase)

        return ofNullable(exception.message)
                .map(::StringBuilder)
                .orElse(builder)
                .toString()
    }

}
