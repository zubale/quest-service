package com.zubale.quest.representation

import com.zubale.quest.representation.enumerator.TokenType
import java.io.Serializable

data class TokenRepresentation(
        val type: TokenType,
        val token: String
) : Serializable
