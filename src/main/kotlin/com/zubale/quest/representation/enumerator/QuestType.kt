package com.zubale.quest.representation.enumerator

enum class QuestType {

    INVENTORY,
    MERCHANDISING,
    MYSTERY_SHOPPER

}