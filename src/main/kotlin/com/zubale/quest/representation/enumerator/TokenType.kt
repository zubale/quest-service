package com.zubale.quest.representation.enumerator

import com.fasterxml.jackson.annotation.JsonValue

enum class TokenType(@get:JsonValue val code: String) {

    BEARER("Bearer"),
    JWT("JWT")

}
