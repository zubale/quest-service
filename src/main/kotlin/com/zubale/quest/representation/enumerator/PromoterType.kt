package com.zubale.quest.representation.enumerator

enum class PromoterType {

    ZUBALE_PROMOTER,
    PROMOTER

}
