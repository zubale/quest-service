package com.zubale.quest.representation

import java.io.Serializable
import java.time.LocalDateTime
import javax.validation.constraints.NotNull

data class CycleRepresentation(
        @get:NotNull
        val startDate: LocalDateTime,

        @get:NotNull
        val endDate: LocalDateTime
) : Serializable
