package com.zubale.quest.representation.request

import javax.validation.constraints.NotNull

data class BrandRequest(

        @get:NotNull
        val zubaleId: Long,

        @get:NotNull
        val fromDate: String,

        @get:NotNull
        val toDate: String
)
