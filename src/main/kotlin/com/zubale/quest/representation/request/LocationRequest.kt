package com.zubale.quest.representation.request

import javax.validation.constraints.NotNull
import javax.validation.constraints.Positive

data class LocationRequest(
        @get:Positive
        val distance: Double?,

        @get:NotNull
        val latitude: Double,

        @get:NotNull
        val longitude: Double
)
