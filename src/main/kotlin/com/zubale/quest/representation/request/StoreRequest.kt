package com.zubale.quest.representation.request

import org.bson.types.ObjectId
import javax.validation.constraints.NotNull

data class StoreRequest(
        val department: ObjectId,

        @get:NotNull
        val storeId: Long
)
