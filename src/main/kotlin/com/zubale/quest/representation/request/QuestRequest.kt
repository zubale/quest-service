package com.zubale.quest.representation.request

import com.zubale.quest.representation.CycleRepresentation
import com.zubale.quest.representation.GoogleFormRepresentation
import com.zubale.quest.representation.enumerator.QuestType
import java.math.BigDecimal
import javax.validation.Valid
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class QuestRequest(
        @get:NotEmpty
        val name: String,

        @get:NotEmpty
        val description: String,

        @get:NotNull
        val brandId: Long,

        @get:Min(0)
        val rewardAmount: BigDecimal,

        @get:NotNull
        val type: QuestType,

        @get:NotEmpty
        val duration: String,

        @get:NotNull
        val cycle: CycleRepresentation,

        @get:NotNull
        val googleForm: GoogleFormRepresentation,

        @Valid
        @get:NotNull
        val stores: List<StoreRequest>,

        val country: String
)
