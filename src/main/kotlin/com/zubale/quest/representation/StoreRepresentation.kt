package com.zubale.quest.representation

import com.zubale.quest.domain.zombie.Retailer
import java.io.Serializable

data class StoreRepresentation(
        val id: Long,
        val name: String? = null,
        val zubaleId: Long,
        val storeNumber: String?,
        val address: String,
        val neighborhood: String?,
        val city: String,
        val state: String,
        val zip: String?,
        val country: String,
        val retailer: Retailer,
        val mapUrl: String?
) : Serializable
