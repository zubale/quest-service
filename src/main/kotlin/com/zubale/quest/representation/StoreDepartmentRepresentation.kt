package com.zubale.quest.representation

import org.bson.types.ObjectId
import java.io.Serializable

data class StoreDepartmentRepresentation(
        val id: ObjectId,
        val name: String
) : Serializable
