package com.zubale.quest.representation

import java.io.Serializable

data class LocationRepresentation(
        val latitude: Double,
        val longitude: Double
) : Serializable
