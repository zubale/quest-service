package com.zubale.quest.representation

import java.io.Serializable

data class BrandRepresentation(
        val id: Long,
        val name: String,
        val zubaleId: Long,
        val logoUrl: String?
) : Serializable
