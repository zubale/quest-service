package com.zubale.quest.representation

import java.io.Serializable

data class GoogleFormRepresentation(
        val url: String,
        val storeFieldKey: String,
        val phoneNumberFieldKey: String,
        val questFieldKey: String
) : Serializable
