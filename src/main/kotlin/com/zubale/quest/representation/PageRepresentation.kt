package com.zubale.quest.representation

import java.io.Serializable

data class PageRepresentation<T>(
        var content: Collection<T>,
        var page: Int,
        var pageSize: Int,
        var totalPages: Int,
        var totalSize: Long
) : Serializable
