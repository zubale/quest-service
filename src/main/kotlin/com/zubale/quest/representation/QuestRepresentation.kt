package com.zubale.quest.representation

import com.fasterxml.jackson.annotation.JsonInclude
import com.zubale.quest.representation.enumerator.QuestType
import org.bson.types.ObjectId
import java.io.Serializable
import java.math.BigDecimal
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
data class QuestRepresentation(
        val id: ObjectId,
        val name: String,
        val type: QuestType,
        val description: String,
        val rewardAmount: BigDecimal,
        val location: LocationRepresentation,
        val distance: Double? = null,
        val googleForm: GoogleFormRepresentation,
        val brand: BrandRepresentation,
        val storeDepartment: String,
        val store: StoreRepresentation,
        val country: String,
        val closed: Boolean,
        val completion: CompletionRepresentation? = null,
        val createdAt: LocalDateTime,
        val updatedAt: LocalDateTime
) : Serializable
