package com.zubale.quest.representation.filter

import com.zubale.quest.util.PageUtil.DEFAULT_PAGE
import com.zubale.quest.util.PageUtil.DEFAULT_PAGE_SIZE
import org.springframework.data.domain.PageRequest

data class PageFilter(
        val page: Int? = DEFAULT_PAGE,
        val pageSize: Int? = DEFAULT_PAGE_SIZE
)

fun PageFilter.toPageRequest(): PageRequest {
    val page = page ?: DEFAULT_PAGE
    val pageSize = pageSize ?: DEFAULT_PAGE_SIZE

    return PageRequest.of(page, pageSize)
}
