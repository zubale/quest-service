package com.zubale.quest.representation

import com.zubale.quest.representation.enumerator.PromoterType
import java.io.Serializable
import java.math.BigDecimal
import java.util.UUID
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

data class CompletionRepresentation(
        @get:NotNull
        val userId: UUID,

        @get:Min(0)
        @get:Max(5000)
        val bonusAmount: BigDecimal,

        val autoPayment: Boolean = true,
        val promoterType: PromoterType
) : Serializable
