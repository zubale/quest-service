package com.zubale.quest.domain

import com.zubale.quest.document.StoreDepartment
import com.zubale.quest.domain.zombie.Brand
import com.zubale.quest.domain.zombie.Store
import com.zubale.quest.representation.BrandRepresentation
import com.zubale.quest.representation.StoreDepartmentRepresentation
import com.zubale.quest.representation.StoreRepresentation
import com.zubale.quest.util.JsonUtil

object ZombieMapper {

    fun brandToRepresentation(brand: Brand): BrandRepresentation {
        return BrandRepresentation(
                id = brand.id,
                zubaleId = brand.zubaleId,
                name = brand.name,
                logoUrl = brand.logoUrl
        )
    }

    fun storeDepartmentToRepresentation(storeDepartment: StoreDepartment): StoreDepartmentRepresentation {
        return StoreDepartmentRepresentation(
                id = storeDepartment.id,
                name = storeDepartment.name
        )
    }

    fun storeToRepresentation(store: Store): StoreRepresentation {
        return StoreRepresentation(
                id = store.id,
                name = store.name,
                storeNumber = store.storeNumber,
                zip = store.zip,
                address = store.address,
                neighborhood = store.neighborhood,
                city = store.city,
                state = store.state,
                country = store.country,
                retailer = store.retailer,
                mapUrl = store.mapUrl,
                zubaleId = store.zubaleId
        )
    }

    fun parseMissingIds(stringModel: String?): String {
        val responseBuilder = StringBuilder("Missing zubale identifiers: ")
        val model = stringModel ?: "{}"
        val node = JsonUtil.jsonStringToNode(model)
        val arrayNode = node.get("missingIds")

        if (arrayNode != null && arrayNode.isArray) {
            arrayNode.asSequence().toList().joinToString()
                    .map { responseBuilder.append(it) }
        }

        return responseBuilder.toString()
    }

}
