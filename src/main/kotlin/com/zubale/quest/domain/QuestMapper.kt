package com.zubale.quest.domain

import com.zubale.quest.document.Quest
import com.zubale.quest.document.StoreDepartment
import com.zubale.quest.domain.ZombieMapper.brandToRepresentation
import com.zubale.quest.domain.ZombieMapper.storeDepartmentToRepresentation
import com.zubale.quest.domain.ZombieMapper.storeToRepresentation
import com.zubale.quest.domain.zombie.Brand
import com.zubale.quest.domain.zombie.Store
import com.zubale.quest.representation.LocationRepresentation
import com.zubale.quest.representation.QuestRepresentation
import com.zubale.quest.representation.request.QuestRequest
import org.springframework.data.mongodb.core.geo.GeoJsonPoint

object QuestMapper {

    fun requestToQuest(questRequest: QuestRequest, brand: Brand, store: Store, department: StoreDepartment): Quest {
        return Quest(
                name = questRequest.name,
                description = questRequest.description,
                type = questRequest.type,
                rewardAmount = questRequest.rewardAmount,
                location = GeoJsonPoint(store.point.longitude, store.point.latitude),
                duration = questRequest.duration,
                cycle = questRequest.cycle,
                googleForm = questRequest.googleForm,
                brand = brandToRepresentation(brand),
                country = questRequest.country,
                department = storeDepartmentToRepresentation(department),
                store = storeToRepresentation(store)
        )
    }

    fun toRepresentation(quest: Quest): QuestRepresentation {
        return QuestRepresentation(
                id = quest.id,
                name = quest.name,
                description = quest.description,
                type = quest.type,
                location = LocationRepresentation(
                        latitude = quest.location.y,
                        longitude = quest.location.x
                ),
                rewardAmount = quest.rewardAmount,
                googleForm = quest.googleForm,
                brand = quest.brand,
                storeDepartment = quest.department.name,
                store = quest.store,
                country = quest.country,
                closed = quest.closed,
                completion = quest.completion,
                createdAt = quest.createdAt,
                updatedAt = quest.createdAt
        )
    }

}
