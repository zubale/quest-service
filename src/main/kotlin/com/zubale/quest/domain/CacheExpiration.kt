package com.zubale.quest.domain

import java.time.temporal.ChronoUnit

class CacheExpiration {
    lateinit var ttlTimeout: String
    lateinit var durationUnit: ChronoUnit
}
