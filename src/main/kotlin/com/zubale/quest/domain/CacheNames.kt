package com.zubale.quest.domain

object CacheNames {

    const val ZOMBIE_AUTH_TOKEN = "zombie-auth-token"
    const val QUEST_LOCATIONS = "quest-locations"
    const val QUEST_BRANDS_FILTER = "quest-brand-filters"
    const val QUEST = "quest"

}
