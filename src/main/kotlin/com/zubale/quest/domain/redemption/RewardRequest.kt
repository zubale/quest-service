package com.zubale.quest.domain.redemption

import java.math.BigDecimal

data class RewardRequest(
        val bonus: BigDecimal,
        val questId: String
)
