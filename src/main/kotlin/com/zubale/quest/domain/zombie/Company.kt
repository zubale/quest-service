package com.zubale.quest.domain.zombie

import java.io.Serializable

data class Company(
        val id: Long,
        val name: String,
        val zubaleId: Long
) : Serializable
