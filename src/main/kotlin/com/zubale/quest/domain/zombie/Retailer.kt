package com.zubale.quest.domain.zombie

import java.io.Serializable

data class Retailer(
        val id: Long,
        val name: String,
        val zubaleId: Long,
        val company: Company,
        val logoUrl: String?
) : Serializable
