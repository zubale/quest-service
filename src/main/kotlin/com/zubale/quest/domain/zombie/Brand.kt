package com.zubale.quest.domain.zombie

import java.io.Serializable

data class Brand(
        val id: Long,
        val name: String,
        val zubaleId: Long,
        val logoUrl: String?
) : Serializable
