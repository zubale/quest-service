package com.zubale.quest.domain.zombie

import java.io.Serializable

data class StorePoint(
        val latitude: Double,
        val longitude: Double
) : Serializable
