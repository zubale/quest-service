package com.zubale.quest.domain.zombie

import java.io.Serializable

data class AuthToken(
        val token: String
) : Serializable
