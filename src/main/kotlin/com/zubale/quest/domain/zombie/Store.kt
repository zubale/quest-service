package com.zubale.quest.domain.zombie

import java.io.Serializable

data class Store(
        val id: Long,
        val zubaleId: Long,
        val name: String? = null,
        val storeNumber: String?,
        val address: String,
        val neighborhood: String?,
        val city: String,
        val state: String,
        val zip: String?,
        val country: String,
        val point: StorePoint,
        val zubaleArea: StoreArea,
        val retailer: Retailer,
        val mapUrl: String?
) : Serializable
