package com.zubale.quest.domain.zombie

import java.io.Serializable

data class StoreArea(
        val id: Long,
        val name: String
) : Serializable
