package com.zubale.quest.domain

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDateTime

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ErrorDetails(
        val message: String? = null,
        val messages: ArrayList<String>? = null,
        val path: String?,
        val at: LocalDateTime? = LocalDateTime.now()
)
